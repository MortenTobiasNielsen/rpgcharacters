﻿using System.Collections.Generic;

namespace RPGCharacters.Items {
    /// <summary>
    /// Represents the possible item slots a character has
    /// </summary>
    public enum ItemSlots {
        HEAD,
        BODY,
        LEGS,
        WEAPON
    }

    /// <summary>
    /// Represents an overall entity in the game e.g. weapons and armor.
    /// </summary>
    public abstract class Item {
        public string Name { get; }
        public int LevelRequirement { get; }
        public ItemSlots Slot { get; }

        public Item(string name, int levelRequirement, ItemSlots itemSlot) {
            Name = name;
            LevelRequirement = levelRequirement;
            Slot = itemSlot;
        }
    }
}
