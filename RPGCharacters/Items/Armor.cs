﻿using RPGCharacters.Attributes;
using System.Collections.Generic;

namespace RPGCharacters.Items {
    /// <summary>
    /// Represents the possible armor types in the game
    /// </summary>
    public enum ArmorTypes {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }

    /// <summary>
    /// <para>Represents a piece of armor in the game.</para>
    /// Armor has name, level requirement, equipped slot, armor type and primary attributes.
    /// </summary>
    public class Armor : Item {
        public PrimaryAttributes PrimaryAttributes { get; }
        public ArmorTypes ArmorType { get; }

        public Armor(string name, int levelRequirement, ItemSlots slot, PrimaryAttributes primaryAttributes, ArmorTypes armorType) : base(name, levelRequirement, slot) {
            PrimaryAttributes = primaryAttributes;
            ArmorType = armorType;
        }
    }
}
