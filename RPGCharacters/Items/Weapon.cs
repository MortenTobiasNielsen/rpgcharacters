﻿namespace RPGCharacters.Items {
    /// <summary>
    /// Represents the possible weapon types in the game
    /// </summary>
    public enum WeaponTypes {
        AXES,
        BOWS,
        DAGGERS,
        HAMMERS,
        STAFFS,
        SWORDS,
        WANDS,
    }

    /// <summary>
    /// <para>Represents a weapon in the game.</para>
    /// weapon has name, level requirement, equipped slot, weapon type, damage, attack speed and DPS.
    /// </summary>
    public class Weapon : Item {
        public int Damage { get; }
        public double AttackSpeed { get; }
        public double DPS { get; }
        public WeaponTypes WeaponType { get; }

        public Weapon(string name, int levelRequirement, ItemSlots itemSlot, int damage, double attackSpeed, WeaponTypes weaponType) : base(name, levelRequirement, itemSlot) {
            Damage = damage;
            AttackSpeed = attackSpeed;
            WeaponType = weaponType;

            DPS = Damage * AttackSpeed;
        }
    }
}
