﻿using RPGCharacters.Attributes;
using RPGCharacters.Items;

namespace RPGCharacters.CharacterClasses {
    /// <summary>
    /// The default amount of primary attributes
    /// </summary>
    public readonly struct RangerPrimaryAttributes {
        public const int Strength = 1;
        public const int Dexterity = 7;
        public const int Intelligence = 1;
        public const int Vitality = 8;
    }

    /// <summary>
    /// The primary attribute increase per level
    /// </summary>
    public readonly struct RangerPrimaryAttributeIncrease {
        public const int StrengthIncrease = 1;
        public const int DexterityIncrease = 5;
        public const int IntelligenceIncrease = 1;
        public const int VitalityIncrease = 2;
    }

    /// <summary>
    /// Represents the ranger character in the game
    /// </summary>
    public class Ranger : BaseCharacterClass {
        private const int StrengthIncrease = RangerPrimaryAttributeIncrease.StrengthIncrease;
        private const int DexterityIncrease = RangerPrimaryAttributeIncrease.DexterityIncrease;
        private const int IntelligenceIncrease = RangerPrimaryAttributeIncrease.IntelligenceIncrease;
        private const int VitalityIncrease = RangerPrimaryAttributeIncrease.VitalityIncrease;

        public Ranger(string name) : base(name) {
            WeaponTypes[] equipableWeaponTypes = new WeaponTypes[] { WeaponTypes.BOWS };
            SetEquipableWeaponTypes(equipableWeaponTypes);

            ArmorTypes[] equipableArmorTypes = new ArmorTypes[] { ArmorTypes.LEATHER, ArmorTypes.MAIL };
            SetEquipableArmorTypes(equipableArmorTypes);

            InitializeEquippedItems();
        }

        public override double GetDPS() {
            Weapon equippedWeapon = (Weapon)EquippedItems[ItemSlots.WEAPON];
            if (equippedWeapon.Damage != 0) {
                return equippedWeapon.DPS * (1 + TotalPrimaryAttributes.Dexterity / 100.0);
            } else {
                return 1;
            }
        }

        public override void LevelUp() {
            Level++;
            BasePrimaryAttributes.Strength += StrengthIncrease;
            BasePrimaryAttributes.Dexterity += DexterityIncrease;
            BasePrimaryAttributes.Intelligence += IntelligenceIncrease;
            BasePrimaryAttributes.Vitality += VitalityIncrease;

            CalculateTotalPrimaryAttributes();
            CalculateSecondaryAttributes();
        }

        protected override PrimaryAttributes SetBasePrimaryAttributes() {
            return new PrimaryAttributes(
                RangerPrimaryAttributes.Strength,
                RangerPrimaryAttributes.Dexterity,
                RangerPrimaryAttributes.Intelligence,
                RangerPrimaryAttributes.Vitality);
        }
    }
}
