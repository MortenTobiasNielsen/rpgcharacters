﻿using RPGCharacters.Attributes;
using RPGCharacters.Items;

namespace RPGCharacters.CharacterClasses {
    /// <summary>
    /// The default amount of primary attributes
    /// </summary>
    public readonly struct RougePrimaryAttributes {
        public const int Strength = 2;
        public const int Dexterity = 6;
        public const int Intelligence = 1;
        public const int Vitality = 8;
    }

    /// <summary>
    /// The primary attribute increase per level
    /// </summary>
    public readonly struct RougePrimaryAttributeIncrease {
        public const int StrengthIncrease = 1;
        public const int DexterityIncrease = 4;
        public const int IntelligenceIncrease = 1;
        public const int VitalityIncrease = 3;
    }

    /// <summary>
    /// Represents the rouge character in the game
    /// </summary>
    public class Rouge : BaseCharacterClass {
        private const int StrengthIncrease = RougePrimaryAttributeIncrease.StrengthIncrease;
        private const int DexterityIncrease = RougePrimaryAttributeIncrease.DexterityIncrease;
        private const int IntelligenceIncrease = RougePrimaryAttributeIncrease.IntelligenceIncrease;
        private const int VitalityIncrease = RougePrimaryAttributeIncrease.VitalityIncrease;

        public Rouge(string name) : base(name) {
            WeaponTypes[] equipableWeaponTypes = new WeaponTypes[] { WeaponTypes.DAGGERS, WeaponTypes.SWORDS };
            SetEquipableWeaponTypes(equipableWeaponTypes);

            ArmorTypes[] equipableArmorTypes = new ArmorTypes[] { ArmorTypes.LEATHER, ArmorTypes.MAIL };
            SetEquipableArmorTypes(equipableArmorTypes);

            InitializeEquippedItems();
        }

        public override double GetDPS() {
            Weapon equippedWeapon = (Weapon)EquippedItems[ItemSlots.WEAPON];
            if (equippedWeapon.Damage != 0) {
                return equippedWeapon.DPS * (1 + TotalPrimaryAttributes.Dexterity / 100.0);
            } else {
                return 1;
            }
        }

        public override void LevelUp() {
            Level++;
            BasePrimaryAttributes.Strength += StrengthIncrease;
            BasePrimaryAttributes.Dexterity += DexterityIncrease;
            BasePrimaryAttributes.Intelligence += IntelligenceIncrease;
            BasePrimaryAttributes.Vitality += VitalityIncrease;

            CalculateTotalPrimaryAttributes();
            CalculateSecondaryAttributes();
        }

        protected override PrimaryAttributes SetBasePrimaryAttributes() {
            return new PrimaryAttributes(
                RougePrimaryAttributes.Strength,
                RougePrimaryAttributes.Dexterity,
                RougePrimaryAttributes.Intelligence,
                RougePrimaryAttributes.Vitality);
        }
    }
}
