﻿using RPGCharacters.Attributes;
using RPGCharacters.Items;

namespace RPGCharacters.CharacterClasses {
    /// <summary>
    /// The default amount of primary attributes
    /// </summary>
    public readonly struct WarriorPrimaryAttributes {
        public const int Strength = 5;
        public const int Dexterity = 2;
        public const int Intelligence = 1;
        public const int Vitality = 10;
    }

    /// <summary>
    /// The primary attribute increase per level
    /// </summary>
    public readonly struct WarriorPrimaryAttributeIncrease {
        public const int StrengthIncrease = 3;
        public const int DexterityIncrease = 2;
        public const int IntelligenceIncrease = 1;
        public const int VitalityIncrease = 5;
    }

    /// <summary>
    /// Represents the warrior character in the game
    /// </summary>
    public class Warrior : BaseCharacterClass {
        private const int StrengthIncrease = WarriorPrimaryAttributeIncrease.StrengthIncrease;
        private const int DexterityIncrease = WarriorPrimaryAttributeIncrease.DexterityIncrease;
        private const int IntelligenceIncrease = WarriorPrimaryAttributeIncrease.IntelligenceIncrease;
        private const int VitalityIncrease = WarriorPrimaryAttributeIncrease.VitalityIncrease;

        public Warrior(string name) : base(name) {
            WeaponTypes[] equipableWeaponTypes = new WeaponTypes[] { WeaponTypes.AXES, WeaponTypes.HAMMERS, WeaponTypes.SWORDS};
            SetEquipableWeaponTypes(equipableWeaponTypes);

            ArmorTypes[] equipableArmorTypes = new ArmorTypes[] { ArmorTypes.MAIL, ArmorTypes.PLATE };
            SetEquipableArmorTypes(equipableArmorTypes);

            InitializeEquippedItems();
        }

        public override double GetDPS() {
            Weapon equippedWeapon = (Weapon)EquippedItems[ItemSlots.WEAPON];
            if (equippedWeapon.Damage > 0) {
                return equippedWeapon.DPS * (1 + TotalPrimaryAttributes.Strength / 100.0);
            } else {
                return 1;
            }
        }

        public override void LevelUp() {
            Level++;
            BasePrimaryAttributes.Strength += StrengthIncrease;
            BasePrimaryAttributes.Dexterity += DexterityIncrease;
            BasePrimaryAttributes.Intelligence += IntelligenceIncrease;
            BasePrimaryAttributes.Vitality += VitalityIncrease;

            CalculateTotalPrimaryAttributes();
            CalculateSecondaryAttributes();
        }

        protected override PrimaryAttributes SetBasePrimaryAttributes() {
            return new PrimaryAttributes(
                WarriorPrimaryAttributes.Strength,
                WarriorPrimaryAttributes.Dexterity,
                WarriorPrimaryAttributes.Intelligence,
                WarriorPrimaryAttributes.Vitality);
        }
    }
}
