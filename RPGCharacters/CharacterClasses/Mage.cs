﻿using RPGCharacters.Attributes;
using RPGCharacters.Items;

namespace RPGCharacters.CharacterClasses {
    /// <summary>
    /// The default amount of primary attributes
    /// </summary>
    public readonly struct MagePrimaryAttributes {
        public const int Strength = 1;
        public const int Dexterity = 1;
        public const int Intelligence = 8;
        public const int Vitality = 5;
    }

    /// <summary>
    /// The primary attribute increase per level
    /// </summary>
    public readonly struct MagePrimaryAttributeIncrease {
        public const int StrengthIncrease = 1;
        public const int DexterityIncrease = 1;
        public const int IntelligenceIncrease = 5;
        public const int VitalityIncrease = 3;
    }

    /// <summary>
    /// Represents the mage character in the game
    /// </summary>
    public class Mage : BaseCharacterClass {
        private const int StrengthIncrease = MagePrimaryAttributeIncrease.StrengthIncrease;
        private const int DexterityIncrease = MagePrimaryAttributeIncrease.DexterityIncrease;
        private const int IntelligenceIncrease = MagePrimaryAttributeIncrease.IntelligenceIncrease;
        private const int VitalityIncrease = MagePrimaryAttributeIncrease.VitalityIncrease;

        public Mage(string name) : base(name) {
            WeaponTypes[] equipableWeaponTypes = new WeaponTypes[] { WeaponTypes.WANDS, WeaponTypes.STAFFS };
            SetEquipableWeaponTypes(equipableWeaponTypes);

            ArmorTypes[] equipableArmorTypes = new ArmorTypes[] { ArmorTypes.CLOTH};
            SetEquipableArmorTypes(equipableArmorTypes);

            InitializeEquippedItems();
        }

        public override double GetDPS() {
            Weapon equippedWeapon = (Weapon)EquippedItems[ItemSlots.WEAPON];
            if (equippedWeapon.Damage != 0) {
                return equippedWeapon.DPS * (1 + TotalPrimaryAttributes.Intelligence / 100.0);
            } else {
                return 1;
            }
        }

        public override void LevelUp() {
            Level++;
            BasePrimaryAttributes.Strength += StrengthIncrease;
            BasePrimaryAttributes.Dexterity += DexterityIncrease;
            BasePrimaryAttributes.Intelligence += IntelligenceIncrease;
            BasePrimaryAttributes.Vitality += VitalityIncrease;

            CalculateTotalPrimaryAttributes();
            CalculateSecondaryAttributes();
        }

        protected override PrimaryAttributes SetBasePrimaryAttributes() {
            return new PrimaryAttributes(
                MagePrimaryAttributes.Strength,
                MagePrimaryAttributes.Dexterity,
                MagePrimaryAttributes.Intelligence,
                MagePrimaryAttributes.Vitality);
        }
    }
}