﻿using RPGCharacters.Attributes;
using RPGCharacters.CustomExceptions;
using RPGCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCharacters.CharacterClasses {
    abstract public class BaseCharacterClass {
        public string Name { get; set; }
        public int Level { get; set; }
        public PrimaryAttributes BasePrimaryAttributes { get; set; }
        public PrimaryAttributes TotalPrimaryAttributes { get; set; }
        public SecondaryAttributes SecondaryAttributes { get; set; } = new SecondaryAttributes();
        public WeaponTypes[] EquipableWeaponTypes { get; set; }
        public ArmorTypes[] EquipableArmorTypes { get; set; }
        public Dictionary<ItemSlots, Item> EquippedItems { get; set; }

        private const int healthIncrease = SecondaryAttributesFactor.health;
        private const int armorRatingIncrease = SecondaryAttributesFactor.armorRating;
        private const int elementalResistanceIncrease = SecondaryAttributesFactor.elementalResistance;

        public BaseCharacterClass(string name) {
            Name = name;
            Level = 1;
            BasePrimaryAttributes = SetBasePrimaryAttributes();
            TotalPrimaryAttributes = InitiateTotalPrimaryAttributes();

            CalculateSecondaryAttributes();
        }

        /// <summary>
        /// Calculates the characters dps
        /// </summary>
        /// <returns>The character dps</returns>
        public abstract double GetDPS();

        /// <summary>
        /// Increase the character level by one,
        /// Increase base attributes,
        /// and recalculates total primary attribates and secondary attributes.
        /// </summary>
        abstract public void LevelUp();

        /// <summary>
        /// Sets the weapon types a specific character can equip
        /// </summary>
        /// <param name="equipableWeaponTypes"></param>
        protected void SetEquipableWeaponTypes(WeaponTypes[] equipableWeaponTypes ) {
            EquipableWeaponTypes = equipableWeaponTypes;
        }

        /// <summary>
        /// Sets the armor types a specifc character can equip
        /// </summary>
        /// <param name="equipableArmorTypes"></param>
        protected void SetEquipableArmorTypes(ArmorTypes[] equipableArmorTypes) {
            EquipableArmorTypes = equipableArmorTypes;
        }

        /// <summary>
        /// Creates a new PrimaryAttributes class and sets it equal to the base primary attributes
        /// </summary>
        /// <returns>The total primary attributes when the character is initiated </returns>
        private PrimaryAttributes InitiateTotalPrimaryAttributes () {
            return new PrimaryAttributes(
                BasePrimaryAttributes.Strength,
                BasePrimaryAttributes.Dexterity,
                BasePrimaryAttributes.Intelligence,
                BasePrimaryAttributes.Vitality);
        }

        /// <summary>
        /// Calculates and updates the secondary attributes
        /// </summary>
        public void CalculateSecondaryAttributes() {
            SecondaryAttributes.Health = healthIncrease * TotalPrimaryAttributes.Vitality;
            SecondaryAttributes.ArmorRating = armorRatingIncrease * (TotalPrimaryAttributes.Strength + TotalPrimaryAttributes.Dexterity);
            SecondaryAttributes.ElementalResistance = elementalResistanceIncrease * TotalPrimaryAttributes.Intelligence;
        }

        /// <summary>
        /// Calculates and update the total primary attributes
        /// </summary>
        protected void CalculateTotalPrimaryAttributes () {
            TotalPrimaryAttributes = BasePrimaryAttributes
                                     + ((Armor)EquippedItems[ItemSlots.HEAD]).PrimaryAttributes
                                     + ((Armor)EquippedItems[ItemSlots.BODY]).PrimaryAttributes
                                     + ((Armor)EquippedItems[ItemSlots.LEGS]).PrimaryAttributes;
        }
        
        /// <summary>
        /// Equips the weapon, if the weapon can be equipped by the character. 
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>Either a success message or an expection</returns>
        public string Equip(Weapon weapon) {
            if (EquipableWeaponTypes.Contains(weapon.WeaponType) && Level >= weapon.LevelRequirement) {
                EquippedItems[ItemSlots.WEAPON] = weapon;
                return Utility.Utility.WeaponEquippedSuccessMessage;
            } 
                
            throw new InvalidWeaponException();
        }

        /// <summary>
        /// Equips the armor, if the armor can be equipped by the character, and updates the primary attributes.
        /// </summary>
        /// <param name="armor"></param>
        /// <returns>Either a success message or an expection</returns>
        public string Equip(Armor armor) {
            if (EquipableArmorTypes.Contains(armor.ArmorType) && Level >= armor.LevelRequirement) {
                EquippedItems[armor.Slot] = armor;
                CalculateTotalPrimaryAttributes();
                return Utility.Utility.ArmorEquippedSuccessMessage;
            } 
         
            throw new InvalidArmorException();
        }

        /// <summary>
        /// Creates dummy armor pieces and a weapon. So the equipment slots aren't null. 
        /// </summary>
        protected void InitializeEquippedItems() {
            PrimaryAttributes defaultArmorAttributes = new(0, 0, 0, 0);

            int levelRequirement = 1;

            Armor defaultHead = new("Default Head", levelRequirement, ItemSlots.HEAD, defaultArmorAttributes, EquipableArmorTypes[0]);
            Armor defaultBody = new("Default Head", levelRequirement, ItemSlots.BODY, defaultArmorAttributes, EquipableArmorTypes[0]);
            Armor defaultLegs = new("Default Head", levelRequirement, ItemSlots.LEGS, defaultArmorAttributes, EquipableArmorTypes[0]);

            int damage = 0;
            double attackSpeed = 0;

            Weapon defaultWeapon = new("Default Weapon", levelRequirement, ItemSlots.WEAPON, damage, attackSpeed, EquipableWeaponTypes[0]);

            EquippedItems = new() {
                { ItemSlots.HEAD, defaultHead },
                { ItemSlots.BODY, defaultBody },
                { ItemSlots.LEGS, defaultLegs },
                { ItemSlots.WEAPON, defaultWeapon }
            };
        }

        /// <summary>
        /// Builds a comma separated string representing the character statistics
        /// </summary>
        /// <returns>A comma seperated string</returns>
        public string GenerateStatistics() {
            StringBuilder sb = new();

            sb.Append($"Name: {Name},\n");
            sb.Append($"Level: {Level},\n");
            sb.Append($"Strength: {TotalPrimaryAttributes.Strength},\n");
            sb.Append($"Dexterity: {TotalPrimaryAttributes.Dexterity},\n");
            sb.Append($"Intelligence: {TotalPrimaryAttributes.Intelligence},\n");
            sb.Append($"Vitality: {TotalPrimaryAttributes.Vitality},\n");
            sb.Append($"Health: {SecondaryAttributes.Health},\n");
            sb.Append($"Armor Rating: {SecondaryAttributes.ArmorRating},\n");
            sb.Append($"Elemental Resistance: {SecondaryAttributes.ElementalResistance},\n");
            sb.Append($"Damge Per Second: {GetDPS()}");

            return sb.ToString();
        }

        /// <summary>
        /// Prints the character statistics
        /// </summary>
        public void PrintStatistics() {
            Console.WriteLine(GenerateStatistics());
        }

        /// <summary>
        /// Setting the base primary attributes
        /// </summary>
        /// <returns>The base primary attributes</returns>
        abstract protected PrimaryAttributes SetBasePrimaryAttributes();
    }
}
