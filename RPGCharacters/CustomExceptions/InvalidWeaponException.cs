﻿using System;

namespace RPGCharacters.CustomExceptions {
    /// <summary>
    /// Provides a custom exception message. 
    /// Should be used when a character tries to equip weapon, which cannot be equipped.
    /// </summary>
    public class InvalidWeaponException : Exception {
        public InvalidWeaponException() {
        }

        public InvalidWeaponException(string message) : base(message) {
        }

        public override string Message => "You cannot equip that waepon";
    }
}
