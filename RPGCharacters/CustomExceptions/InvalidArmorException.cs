﻿using System;

namespace RPGCharacters.CustomExceptions {
    /// <summary>
    /// Provides a custom exception message. 
    /// Should be used when a character tries to equip armor, which it cannot equipped.
    /// </summary>
    public class InvalidArmorException : Exception {
        public InvalidArmorException() {
        }

        public InvalidArmorException(string message) : base(message) {
        }

        public override string Message => "You cannot equip this armor";
    }
}
