﻿namespace RPGCharacters.Utility {
    /// <summary>
    /// Should be used to provide success messages when equipping equipment
    /// </summary>
    public static class Utility {
        public const string WeaponEquippedSuccessMessage = "New weapon equipped!";
        public const string ArmorEquippedSuccessMessage = "New Armor equipped!";
    }
}
