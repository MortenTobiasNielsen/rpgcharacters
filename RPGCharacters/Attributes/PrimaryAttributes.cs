﻿using System;

namespace RPGCharacters.Attributes {
    /// <summary>
    /// Represents the primary attributes of an entity in the game
    /// </summary>
    public class PrimaryAttributes {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Vitality { get; set; }

        public PrimaryAttributes(int defaultStrength, int defaultDexterity, int defaultIntelligence, int defaultVitality) {
            Strength = defaultStrength;
            Dexterity = defaultDexterity;
            Intelligence = defaultIntelligence;
            Vitality = defaultVitality;
        }

        public override bool Equals(object obj) {
            return obj is PrimaryAttributes attributes &&
                   Strength == attributes.Strength &&
                   Dexterity == attributes.Dexterity &&
                   Intelligence == attributes.Intelligence &&
                   Vitality == attributes.Vitality;
        }

        public override int GetHashCode() {
            return HashCode.Combine(Strength, Dexterity, Intelligence, Vitality);
        }

        public static PrimaryAttributes operator +(PrimaryAttributes lhs, PrimaryAttributes rhs) {
            return new PrimaryAttributes(
                lhs.Strength + rhs.Strength,
                lhs.Dexterity + rhs.Dexterity,
                lhs.Intelligence + rhs.Intelligence,
                lhs.Vitality + rhs.Vitality);
        }
    }
}
