﻿using System;

namespace RPGCharacters.Attributes {
    /// <summary>
    /// <para>The factors use to calculate the secondary attributes.</para>
    /// Health, Armor Rating, and Elemental Resistance
    /// </summary>
    public struct SecondaryAttributesFactor {
        public const int health = 10;
        public const int armorRating = 1;
        public const int elementalResistance = 1;
    }

    /// <summary>
    /// Represents the secondary attributes of an entity in the game
    /// </summary>
    public class SecondaryAttributes {
        public int Health { get; set; }
        public int ArmorRating { get; set; }
        public int ElementalResistance { get; set; }

        public SecondaryAttributes() {
        }

        public override bool Equals(object obj) {
            return obj is SecondaryAttributes attributes &&
                   Health == attributes.Health &&
                   ArmorRating == attributes.ArmorRating &&
                   ElementalResistance == attributes.ElementalResistance;
        }

        public override int GetHashCode() {
            return HashCode.Combine(Health, ArmorRating, ElementalResistance);
        }
    }
}
