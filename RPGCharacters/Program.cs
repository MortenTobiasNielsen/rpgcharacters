﻿using RPGCharacters.CharacterClasses;
using System;

namespace RPGCharacters {
    class Program {
        static void Main() {
            // Just to see the statistics print out
            Mage mage = new("Test");
            mage.PrintStatistics();
        }
    }
}
