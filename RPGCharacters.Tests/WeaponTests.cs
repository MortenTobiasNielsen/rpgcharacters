﻿using Xunit;
using RPGCharacters.CharacterClasses;
using RPGCharacters.CustomExceptions;
using RPGCharacters.Items;

namespace RPGCharacters.Tests {
    public class WeaponTests {
        [Fact]
        public void Equip_CreateWarriorEquipWeapon_ShouldReturnSuccesMessage() {
            // Arrange
            Warrior warrior = new("Test");
            int damage = 7;
            double attackSpeed = 1.1;
            int levelRequirement = 1;

            Weapon testAxe = new("Common axe", levelRequirement, ItemSlots.WEAPON, damage, attackSpeed, WeaponTypes.AXES);

            string expected = Utility.Utility.WeaponEquippedSuccessMessage;

            // Act
            string actual = warrior.Equip(testAxe);

            // Act and Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equip_CreateWarriorEquipWeaponWithWrongType_ShouldReturnInvalidWeaponException() {
            // Arrange
            Warrior warrior = new("Test");
            int damage = 7;
            double attackSpeed = 1.1;
            int levelRequirement = 1;

            Weapon testBow = new("Common bow", levelRequirement, ItemSlots.WEAPON, damage, attackSpeed, WeaponTypes.BOWS);

            // Act and Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.Equip(testBow));
        }

        [Fact]
        public void Equip_CreateWarriorEquipWeaponWithTooHighLevel_ShouldReturnInvalidWeaponException() {
            // Arrange
            Warrior warrior = new("Test");
            int damage = 7;
            double attackSpeed = 1.1;
            int levelRequirement = 2;

            Weapon testAxe = new("Common axe", levelRequirement, ItemSlots.WEAPON, damage, attackSpeed, WeaponTypes.AXES);

            // Act and Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.Equip(testAxe));
        }
    }
}
