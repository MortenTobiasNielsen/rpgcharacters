﻿using Xunit;
using RPGCharacters.Attributes;
using RPGCharacters.CharacterClasses;
using RPGCharacters.Items;

namespace RPGCharacters.Tests {
    public class MageTests {
        [Fact]
        public void BasePrimaryAttributes_CheckTotalPrimaryAttributes_ShouldReturnDefaultPrimaryAttributes() {
            // Arrange
            Mage mage = new("test");
            PrimaryAttributes expected = new(
                MagePrimaryAttributes.Strength,
                MagePrimaryAttributes.Dexterity,
                MagePrimaryAttributes.Intelligence,
                MagePrimaryAttributes.Vitality);

            // Act
            PrimaryAttributes actual = mage.BasePrimaryAttributes;

            // Assert
            Assert.True(expected.Equals(actual));
        }

        [Fact]
        public void PrimaryAttributesIncrease_CheckPrimaryAttributesAfterLevelUp_ShouldReturnIncreasedPrimaryAttributes() {
            // Arrange
            Mage mage = new("test");

            int levelUps = 1;

            int totalStrength = MagePrimaryAttributes.Strength + MagePrimaryAttributeIncrease.StrengthIncrease * levelUps;
            int totalDexterity = MagePrimaryAttributes.Dexterity + MagePrimaryAttributeIncrease.DexterityIncrease * levelUps;
            int totalIntelligence = MagePrimaryAttributes.Intelligence + MagePrimaryAttributeIncrease.IntelligenceIncrease * levelUps;
            int totalVitality = MagePrimaryAttributes.Vitality + MagePrimaryAttributeIncrease.VitalityIncrease * levelUps;

            PrimaryAttributes expected = new(
                totalStrength,
                totalDexterity,
                totalIntelligence,
                totalVitality);

            // Act
            mage.LevelUp();
            PrimaryAttributes actual = mage.TotalPrimaryAttributes;

            // Assert
            Assert.True(expected.Equals(actual));
        }

        [Fact]
        public void GetDPS_NoWeaponEquipped_ShouldReturnOne() {
            // Arrange
            Mage mage = new("Test");

            double expected = 1;

            // Act
            double actual = mage.GetDPS();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDPS_WithWeaponEquip_ShouldReturnCorrectDPS() {
            // Arrange
            Mage mage = new("Test");
            int levelRequirement = 1;
            int damage = 10;
            double attackSpeed = 0.91;

            Weapon testWand = new("Common Wand", levelRequirement, ItemSlots.WEAPON, damage, attackSpeed, WeaponTypes.WANDS);

            double DPSMultiplier = 1 + MagePrimaryAttributes.Intelligence / 100.0;
            double expected = testWand.DPS * DPSMultiplier;

            // Act
            mage.Equip(testWand);
            double actual = mage.GetDPS();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDPS_WithWeaponAndArmorEquipped_ShouldReturnCorrectDPS() {
            // Arrange
            Mage mage = new("Test");
            int levelRequirement = 1;
            int damage = 10;
            double attackSpeed = 0.91;

            Weapon testWand = new("Common Wand", levelRequirement, ItemSlots.WEAPON, damage, attackSpeed, WeaponTypes.WANDS);

            const int Strength = 0;
            const int Dexterity = 0;
            const int Intelligence = 2;
            const int Vitality = 1;
            PrimaryAttributes testPrimaryAttributes = new(Strength, Dexterity, Intelligence, Vitality);

            Armor testClothHead = new("Common cloth head armor", levelRequirement, ItemSlots.HEAD, testPrimaryAttributes, ArmorTypes.CLOTH);

            double DPSMultiplier = 1 + (MagePrimaryAttributes.Intelligence + Intelligence) / 100.0;
            double expected = testWand.DPS * DPSMultiplier;

            // Act
            mage.Equip(testWand);
            mage.Equip(testClothHead);
            double actual = mage.GetDPS();

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
