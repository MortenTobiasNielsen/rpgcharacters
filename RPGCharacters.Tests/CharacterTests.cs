using Xunit;
using RPGCharacters.Attributes;
using RPGCharacters.CharacterClasses;

namespace RPGCharacters.Tests {
    public class GeneralCharacterTests {
        [Fact]
        public void Constructor_InitializeWithLevelOne_ShouldReturnLevelOne() {
            // Arrange
            Mage mage = new("test");
            int expected = 1;

            // Act
            int actual = mage.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_CreateMageAndLevelUpOnce_ShouldReturnLevelTwo() {
            // Arrange
            Mage mage = new("test");
            int expected = 2;

            // Act
            mage.LevelUp();
            int actual = mage.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void SecondaryAttributesCalculation_CreateMageAndCheckSecondaryAttributesAfterLevelUp_ShouldReturnCalculatedSecondaryAttributes() {
            // Arrange
            Mage mage = new("test");
            int levelUps = 1;

            int totalStrength = MagePrimaryAttributes.Strength + MagePrimaryAttributeIncrease.StrengthIncrease * levelUps;
            int totalDexterity = MagePrimaryAttributes.Dexterity + MagePrimaryAttributeIncrease.DexterityIncrease * levelUps;
            int totalIntelligence = MagePrimaryAttributes.Intelligence + MagePrimaryAttributeIncrease.IntelligenceIncrease * levelUps;
            int totalVitality = MagePrimaryAttributes.Vitality + MagePrimaryAttributeIncrease.VitalityIncrease * levelUps;

            SecondaryAttributes expected = new();
            expected.Health = totalVitality * SecondaryAttributesFactor.health;
            expected.ArmorRating = (totalStrength + totalDexterity) * SecondaryAttributesFactor.armorRating;
            expected.ElementalResistance = totalIntelligence * SecondaryAttributesFactor.elementalResistance;

            // Act
            mage.LevelUp();
            SecondaryAttributes actual = mage.SecondaryAttributes;

            // Assert
            Assert.True(expected.Equals(actual));
        }

        [Fact]
        public void PrintStatistics_CreateCharacter_ShouldReturnCharacterStatistics() {
            // Arrange
            string name = "Test";
            Mage mage = new(name);

            int level = 1;
            int health = MagePrimaryAttributes.Vitality * SecondaryAttributesFactor.health;
            int armorRating = (MagePrimaryAttributes.Strength + MagePrimaryAttributes.Dexterity) * SecondaryAttributesFactor.armorRating;
            int elementalResistance = MagePrimaryAttributes.Intelligence * SecondaryAttributesFactor.elementalResistance;
            double DPS = 1;

            string expected = 
                $"Name: {name},\n" +
                $"Level: {level},\n" +
                $"Strength: {MagePrimaryAttributes.Strength},\n" +
                $"Dexterity: {MagePrimaryAttributes.Dexterity},\n" +
                $"Intelligence: {MagePrimaryAttributes.Intelligence},\n" +
                $"Vitality: {MagePrimaryAttributes.Vitality},\n" +
                $"Health: {health},\n" +
                $"Armor Rating: {armorRating},\n" +
                $"Elemental Resistance: {elementalResistance},\n" +
                $"Damge Per Second: {DPS}";

            // Act
            string actual = mage.GenerateStatistics();

            // Asset
            Assert.Equal(expected, actual);
        }
    }
}
