﻿using Xunit;
using RPGCharacters.Attributes;
using RPGCharacters.CharacterClasses;
using RPGCharacters.Items;

namespace RPGCharacters.Tests {
    public class WarriorTests {
        [Fact]
        public void BasePrimaryAttributes_CheckTotalPrimaryAttributes_ShouldReturnDefaultPrimaryAttributes() {
            // Arrange
            Warrior warrior = new("test");
            PrimaryAttributes expected = new(
                WarriorPrimaryAttributes.Strength,
                WarriorPrimaryAttributes.Dexterity,
                WarriorPrimaryAttributes.Intelligence,
                WarriorPrimaryAttributes.Vitality);

            // Act
            PrimaryAttributes actual = warrior.BasePrimaryAttributes;

            // Assert
            Assert.True(expected.Equals(actual));
        }

        [Fact]
        public void PrimaryAttributesIncrease_CheckPrimaryAttributesAfterLevelUp_ShouldReturnIncreasedPrimaryAttributes() {
            // Arrange
            Warrior warrior = new("test");

            int levelUps = 1;

            int totalStrength = WarriorPrimaryAttributes.Strength + WarriorPrimaryAttributeIncrease.StrengthIncrease * levelUps;
            int totalDexterity = WarriorPrimaryAttributes.Dexterity + WarriorPrimaryAttributeIncrease.DexterityIncrease * levelUps;
            int totalIntelligence = WarriorPrimaryAttributes.Intelligence + WarriorPrimaryAttributeIncrease.IntelligenceIncrease * levelUps;
            int totalVitality = WarriorPrimaryAttributes.Vitality + WarriorPrimaryAttributeIncrease.VitalityIncrease * levelUps;

            PrimaryAttributes expected = new(
                totalStrength,
                totalDexterity,
                totalIntelligence,
                totalVitality);

            // Act
            warrior.LevelUp();
            PrimaryAttributes actual = warrior.TotalPrimaryAttributes;

            // Assert
            Assert.True(expected.Equals(actual));
        }

        [Fact]
        public void GetDPS_NoWeaponEquipped_ShouldReturnOne() {
            // Arrange
            Warrior warrior = new("Test");

            double expected = 1;

            // Act
            double actual = warrior.GetDPS();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDPS_WithWeaponEquip_ShouldReturnCorrectDPS() {
            // Arrange
            Warrior warrior = new("Test");
            int damage = 7;
            double attackSpeed = 1.1;
            int levelRequirement = 1;

            Weapon testAxe = new("Common axe", levelRequirement, ItemSlots.WEAPON, damage, attackSpeed, WeaponTypes.AXES);

            double DPSMultiplier = 1 + WarriorPrimaryAttributes.Strength / 100.0;
            double expected = testAxe.DPS * DPSMultiplier;

            // Act
            warrior.Equip(testAxe);
            double actual = warrior.GetDPS();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDPS_WithWeaponAndArmorEquipped_ShouldReturnCorrectDPS() {
            // Arrange
            Warrior warrior = new("Test");
            int levelRequirement = 1;
            int damage = 7;
            double attackSpeed = 1.1;

            Weapon testAxe = new("Common axe", levelRequirement, ItemSlots.WEAPON, damage, attackSpeed, WeaponTypes.AXES);

            const int strength = 1;
            const int dexterity = 0;
            const int intelligence = 0;
            const int vitality = 2;
            PrimaryAttributes testPrimaryAttributes = new(strength, dexterity, intelligence, vitality);

            Armor testPlateBody = new("Common plate body armor", levelRequirement, ItemSlots.BODY, testPrimaryAttributes, ArmorTypes.PLATE);

            double DPSMultiplier = 1 + (WarriorPrimaryAttributes.Strength + strength) / 100.0;
            double expected = testAxe.DPS * DPSMultiplier;

            // Act
            warrior.Equip(testAxe);
            warrior.Equip(testPlateBody);
            double actual = warrior.GetDPS();

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
