﻿using Xunit;
using System;
using RPGCharacters.Attributes;
using RPGCharacters.Items;
using RPGCharacters.CharacterClasses;

namespace RPGCharacters.Tests {
    public class RangerTests {
        [Fact]
        public void BasePrimaryAttributes_CheckTotalPrimaryAttributes_ShouldReturnDefaultPrimaryAttributes() {
            // Arrange
            Ranger ranger = new("test");
            PrimaryAttributes expected = new(
                RangerPrimaryAttributes.Strength,
                RangerPrimaryAttributes.Dexterity,
                RangerPrimaryAttributes.Intelligence,
                RangerPrimaryAttributes.Vitality);

            // Act
            PrimaryAttributes actual = ranger.BasePrimaryAttributes;

            // Assert
            Assert.True(expected.Equals(actual));
        }

        [Fact]
        public void PrimaryAttributesIncrease_CheckPrimaryAttributesAfterLevelUp_ShouldReturnIncreasedPrimaryAttributes() {
            // Arrange
            Ranger ranger = new("test");

            int levelUps = 1;

            int totalStrength = RangerPrimaryAttributes.Strength + RangerPrimaryAttributeIncrease.StrengthIncrease * levelUps;
            int totalDexterity = RangerPrimaryAttributes.Dexterity + RangerPrimaryAttributeIncrease.DexterityIncrease * levelUps;
            int totalIntelligence = RangerPrimaryAttributes.Intelligence + RangerPrimaryAttributeIncrease.IntelligenceIncrease * levelUps;
            int totalVitality = RangerPrimaryAttributes.Vitality + RangerPrimaryAttributeIncrease.VitalityIncrease * levelUps;

            PrimaryAttributes expected = new(
                totalStrength,
                totalDexterity,
                totalIntelligence,
                totalVitality);

            // Act
            ranger.LevelUp();
            PrimaryAttributes actual = ranger.TotalPrimaryAttributes;

            // Assert
            Assert.True(expected.Equals(actual));
        }

        [Fact]
        public void GetDPS_NoWeaponEquipped_ShouldReturnOne() {
            // Arrange
            Ranger ranger = new("Test");

            double expected = 1;

            // Act
            double actual = ranger.GetDPS();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDPS_WithWeaponEquip_ShouldReturnCorrectDPS() {
            // Arrange
            Ranger ranger = new("Test");
            int levelRequirement = 1;
            int damage = 15;
            double attackSpeed = 0.75;

            Weapon testBow = new("Common Bow", levelRequirement, ItemSlots.WEAPON, damage, attackSpeed, WeaponTypes.BOWS);

            double DPSMultiplier = 1 + RangerPrimaryAttributes.Dexterity / 100.0;
            double expected = testBow.DPS * DPSMultiplier;

            // Act
            ranger.Equip(testBow);
            double actual = ranger.GetDPS();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDPS_WithWeaponAndArmorEquipped_ShouldReturnCorrectDPS() {
            // Arrange
            Ranger ranger = new("Test");
            int levelRequirement = 1;
            int damage = 15;
            double attackSpeed = 0.75;

            Weapon testBow = new("Common Bow", levelRequirement, ItemSlots.WEAPON, damage, attackSpeed, WeaponTypes.BOWS);

            const int Strength = 0;
            const int Dexterity = 3;
            const int Intelligence = 0;
            const int Vitality = 2;
            PrimaryAttributes testPrimaryAttributes = new(Strength, Dexterity, Intelligence, Vitality);

            Armor testLetherHead = new("Common cloth head armor", levelRequirement, ItemSlots.HEAD, testPrimaryAttributes, ArmorTypes.LEATHER);

            double DPSMultiplier = 1 + (RangerPrimaryAttributes.Dexterity + Dexterity) / 100.0;
            double expected = testBow.DPS * DPSMultiplier;

            // Act
            ranger.Equip(testBow);
            ranger.Equip(testLetherHead);
            double actual = ranger.GetDPS();

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
