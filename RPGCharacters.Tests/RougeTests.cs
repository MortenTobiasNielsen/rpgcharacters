﻿using RPGCharacters.Attributes;
using RPGCharacters.CharacterClasses;
using RPGCharacters.Items;
using Xunit;

namespace RPGCharacters.Tests {
    public class RougeTests {
        [Fact]
        public void BasePrimaryAttributes_CheckTotalPrimaryAttributes_ShouldReturnDefaultPrimaryAttributes() {
            // Arrange
            Rouge rouge = new("test");
            PrimaryAttributes expected = new(
                RougePrimaryAttributes.Strength,
                RougePrimaryAttributes.Dexterity,
                RougePrimaryAttributes.Intelligence,
                RougePrimaryAttributes.Vitality);

            // Act
            PrimaryAttributes actual = rouge.BasePrimaryAttributes;

            // Assert
            Assert.True(expected.Equals(actual));
        }

        [Fact]
        public void PrimaryAttributesIncrease_CheckPrimaryAttributesAfterLevelUp_ShouldReturnIncreasedPrimaryAttributes() {
            // Arrange
            Rouge rouge = new("test");

            int levelUps = 1;

            int totalStrength = RougePrimaryAttributes.Strength + RougePrimaryAttributeIncrease.StrengthIncrease * levelUps;
            int totalDexterity = RougePrimaryAttributes.Dexterity + RougePrimaryAttributeIncrease.DexterityIncrease * levelUps;
            int totalIntelligence = RougePrimaryAttributes.Intelligence + RougePrimaryAttributeIncrease.IntelligenceIncrease * levelUps;
            int totalVitality = RougePrimaryAttributes.Vitality + RougePrimaryAttributeIncrease.VitalityIncrease * levelUps;

            PrimaryAttributes expected = new(
                totalStrength,
                totalDexterity,
                totalIntelligence,
                totalVitality);

            // Act
            rouge.LevelUp();
            PrimaryAttributes actual = rouge.TotalPrimaryAttributes;

            // Assert
            Assert.True(expected.Equals(actual));
        }

        [Fact]
        public void GetDPS_NoWeaponEquipped_ShouldReturnOne() {
            // Arrange
            Rouge rouge = new("Test");

            double expected = 1;

            // Act
            double actual = rouge.GetDPS();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDPS_WithWeaponEquip_ShouldReturnCorrectDPS() {
            // Arrange
            Rouge rouge = new("Test");
            int levelRequirement = 1;
            int damage = 4;
            double attackSpeed = 2.5;

            Weapon testDagger = new("Common Dagger", levelRequirement, ItemSlots.WEAPON, damage, attackSpeed, WeaponTypes.DAGGERS);

            double DPSMultiplier = 1 + RougePrimaryAttributes.Dexterity / 100.0;
            double expected = testDagger.DPS * DPSMultiplier;

            // Act
            rouge.Equip(testDagger);
            double actual = rouge.GetDPS();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDPS_WithWeaponAndArmorEquipped_ShouldReturnCorrectDPS() {
            // Arrange
            Rouge rouge = new("Test");
            int levelRequirement = 1;
            int damage = 4;
            double attackSpeed = 2.5;

            Weapon testDagger = new("Common Dagger", levelRequirement, ItemSlots.WEAPON, damage, attackSpeed, WeaponTypes.DAGGERS);

            const int Strength = 0;
            const int Dexterity = 2;
            const int Intelligence = 0;
            const int Vitality = 1;
            PrimaryAttributes testPrimaryAttributes = new(Strength, Dexterity, Intelligence, Vitality);

            Armor testLeatherLegs = new("Common leather legs", levelRequirement, ItemSlots.LEGS, testPrimaryAttributes, ArmorTypes.LEATHER);

            double DPSMultiplier = 1 + (RougePrimaryAttributes.Dexterity + Dexterity) / 100.0;
            double expected = testDagger.DPS * DPSMultiplier;

            // Act
            rouge.Equip(testDagger);
            rouge.Equip(testLeatherLegs);
            double actual = rouge.GetDPS();

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
