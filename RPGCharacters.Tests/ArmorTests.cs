﻿using Xunit;
using RPGCharacters.Attributes;
using RPGCharacters.CharacterClasses;
using RPGCharacters.CustomExceptions;
using RPGCharacters.Items;

namespace RPGCharacters.Tests {
    public class ArmorTests {
        [Fact]
        public void Equip_CreateWarriorEquipArmor_ShouldReturnSuccesMessage() {
            // Arrange
            Warrior warrior = new("Test");
            const int levelRequirement = 1;
            const int strength = 1;
            const int dexterity = 0;
            const int intelligence = 0;
            const int vitality = 2;
            PrimaryAttributes testPrimaryAttributes = new(strength, dexterity, intelligence, vitality);

            Armor testPlateBody = new("Common plate body armor", levelRequirement, ItemSlots.BODY, testPrimaryAttributes, ArmorTypes.PLATE);

            string expected = Utility.Utility.ArmorEquippedSuccessMessage;

            // Act
            string actual = warrior.Equip(testPlateBody);

            // Act and Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equip_CreateWarriorEquipArmorWithWrongType_ShouldReturnInvalidArmorException() {
            // Arrange
            Warrior warrior = new("Test");
            const int levelRequirement = 1;
            const int strength = 1;
            const int dexterity = 0;
            const int intelligence = 0;
            const int vitality = 2;
            PrimaryAttributes testPrimaryAttributes = new(strength, dexterity, intelligence, vitality);

            Armor testClothHead = new("Common cloth head armor", levelRequirement, ItemSlots.HEAD, testPrimaryAttributes, ArmorTypes.CLOTH);

            // Act and Assert
            Assert.Throws<InvalidArmorException>(() => warrior.Equip(testClothHead));
        }

        [Fact]
        public void Equip_CreateWarriorEquipArmorWithTooHighLevel_ShouldReturnInvalidArmorException() {
            // Arrange
            Warrior warrior = new("Test");
            const int levelRequirement = 2;
            const int strength = 1;
            const int dexterity = 0;
            const int intelligence = 0;
            const int vitality = 2;
            PrimaryAttributes testPrimaryAttributes = new(strength, dexterity, intelligence, vitality);

            Armor testPlateBody = new("Common plate body armor", levelRequirement, ItemSlots.BODY, testPrimaryAttributes, ArmorTypes.PLATE);

            // Act and Assert
            Assert.Throws<InvalidArmorException>(() => warrior.Equip(testPlateBody));
        }
    }
}
